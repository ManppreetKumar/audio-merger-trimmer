var exec = require('cordova/exec');

var PLUGIN_NAME = "audioMergerTrimmer"; // This is just for code completion uses.

var audioMergerTrimmer = function() {}; // This just makes it easier forus to export all of the functions at once.
// All of your plugin functions go below this. 
// Note: We are not passing any options in the [] block for this, so make sure you include the empty [] block.
audioMergerTrimmer.merge = function(arguments, onSuccess, onError) {
   exec(onSuccess, onError, PLUGIN_NAME, "merge", arguments);
};

audioMergerTrimmer.trim = function(arguments, onSuccess, onError) {
   exec(onSuccess, onError, PLUGIN_NAME, "trim", arguments);
};

module.exports = audioMergerTrimmer;
