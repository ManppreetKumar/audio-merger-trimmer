package org.plugin;
import org.apache.cordova.CordovaPlugin;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import android.media.MediaMetadataRetriever;
import android.util.Log;
import java.io.File;
import java.io.InputStream;
import javax.inject.Inject;
import java.io.OutputStream;
import java.io.IOException;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

public class audioMergerTrimmer extends CordovaPlugin {

    @Inject
    FFmpeg ffmpeg;

    @Override
    public void initialize(final CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if ("merge".equals(action)) {
            merge(args.getString(0), args.getString(1), args.getString(2), args.getString(3),args.getString(4), callbackContext);
            return true;
        }
        if ("trim".equals(action)) {
            trim(args.getString(0), args.getString(1), args.getInt(2), args.getString(3), callbackContext);
            return true;
        }
        return false;
    }

    private void showUnsupportedExceptionDialog() {
        Log.d("MNpoty","Not supported");
    }

    private void merge(String file1, String file2, String file3, String file4, String textFile, CallbackContext callbackContext) {
        try {
            try {
                FFmpeg ffmpeg = FFmpeg.getInstance(this.cordova.getActivity().getApplicationContext());
                ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                    @Override
                    public void onFailure() {
                        callbackContext.error("Audio trimming is not supported on your phone.");
                    }
                    @Override
                    public void onSuccess() {
                        try {
                            String text = "-y -i "+file1+" "+file3;
                            if(!file2.isEmpty()) {
                                Log.d("Please check Log",file2);
                                text = "-y -i "+file2+" -qscale:a 3 "+textFile+"temp.mp3";
                            }
                            String[] commands = text.split(" ");
                            ffmpeg.execute(commands, new ExecuteBinaryResponseHandler() {
                                @Override
                                public void onFailure(String message) {
                                    Log.d("Fialed",message);
                                    callbackContext.error(message);
                                }

                                @Override   
                                public void onSuccess(String message) {
                                    if(!file2.isEmpty()) {
                                        try {
                                            Log.d("Please check file3",file3);
                                            String text2 = "-f concat -i " + textFile + "inputfiles.txt -acodec copy " + file3;
                                            String[] commands2 = text2.split(" ");
                                            ffmpeg.execute(commands2, new ExecuteBinaryResponseHandler() {
                                                @Override
                                                public void onFailure(String message) {
                                                    Log.d("Fialed",message);
                                                    callbackContext.error(message);
                                                }

                                                @Override
                                                public void onSuccess(String message) {
                                                    try {
                                                        JSONObject item = new JSONObject();
                                                        item.put("filePath", file3);
                                                        item.put("tempPath",file4);
                                                        callbackContext.success(item);
                                                    } catch (Exception e) {
                                                        callbackContext.error(e.getLocalizedMessage());
                                                    }
                                                }
                                            });
                                        } catch (Exception e) {
                                            callbackContext.error(e.getLocalizedMessage());
                                        }
                                    } else {
                                        try {
                                            JSONObject item = new JSONObject();
                                            item.put("filePath", file3);
                                            item.put("tempPath",file4);
                                            callbackContext.success(item);
                                        } catch (Exception e) {
                                            callbackContext.error(e.getLocalizedMessage());
                                        }
                                    }
                                }
                            });
                        } catch (FFmpegCommandAlreadyRunningException e) {
                            callbackContext.error(e.getLocalizedMessage());
                        }
                    }
                });
            } catch (FFmpegNotSupportedException e) {
                callbackContext.error(e.getLocalizedMessage());
            }
        } catch(Exception e) {
            callbackContext.error(e.getLocalizedMessage());
        } finally {
        }
    }

    private void trim(String file1, String file2, int duration, String tempPath, CallbackContext callbackContext)  {
        try {
            try {
                FFmpeg ffmpeg = FFmpeg.getInstance(this.cordova.getActivity().getApplicationContext());
                ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                    @Override
                    public void onFailure() {
                        callbackContext.error("Audio trimming is not supported on your phone.");
                    }
                    @Override
                    public void onSuccess() {
                        try {
                            // to execute "ffmpeg -version" command you just need to pass "-version"
                            String text = "-y -i "+file1+" -ss 00 -to "+Math.round(duration/1000)+" -c copy "+file2;
                            String[] commands = text.split(" ");
                            ffmpeg.execute(commands, new ExecuteBinaryResponseHandler() {
                                @Override
                                public void onFailure(String message) {
                                    callbackContext.error(message);
                                }

                                @Override
                                public void onSuccess(String message) {
                                    try {
                                        Log.d("Success",String.valueOf(message));
                                        JSONObject item = new JSONObject();
                                        item.put("filePath", file2);
                                        item.put("tempPath",tempPath);
                                        callbackContext.success(item);
                                    } catch (Exception e) {
                                        callbackContext.error(e.getLocalizedMessage());
                                    }
                                }
                            });
                        } catch (FFmpegCommandAlreadyRunningException e) {
                            callbackContext.error(e.getLocalizedMessage());
                        }
                    }

                });
            } catch (FFmpegNotSupportedException e) {
                callbackContext.error(e.getLocalizedMessage());
            }
        } catch(Exception e) {
            callbackContext.error(e.getLocalizedMessage());
        } finally {
        }
    }

}