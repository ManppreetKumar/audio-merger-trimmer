import Foundation
import AVFoundation
/*
* Notes: The @objc shows that this class & function should be exposed to Cordova.
*/
@objc(audioMergerTrimmer) class audioMergerTrimmer : CDVPlugin {
  @objc(merge:) // Declare your function name.
  func merge(command: CDVInvokedUrlCommand) {
    var pluginResult = CDVPluginResult (status: CDVCommandStatus_ERROR, messageAs: "The Plugin Failed");
    print(command.arguments);
    // write the function code.
    let audio1 = command.arguments[0] as? String ?? ""
    let audio2 = command.arguments[1] as? String ?? ""
    let audio3 = command.arguments[2] as? String ?? ""
    let tempPath = command.arguments[3] as? String ?? ""
    var error = "";
    if(audio1=="") {
        error = "Audio 1 url is missing or invalid.";
    }
    if(audio3=="") {
        error = "Audio 3 url is missing or invalid.";
    }
    if(tempPath=="") {
        error = "Temp Path url is missing or invalid.";
    }
    if(error != "") {
        pluginResult = CDVPluginResult (status: CDVCommandStatus_ERROR, messageAs: error);
        self.commandDelegate!.send(pluginResult, callbackId: command.callbackId);
    }
    let url1 = URL(string: audio1)!
    let url3 = URL(string: audio3)!
    
    let composition = AVMutableComposition()
    let compositionAudioTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)
    compositionAudioTrack!.append(url: url1)
    if(audio2 != "") {
        let url2 = URL(string: audio2)!
        compositionAudioTrack!.append(url: url2)
    }
    if let assetExport = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetAppleM4A) {
        assetExport.outputFileType = AVFileType.m4a;
        assetExport.outputURL = url3;
        assetExport.exportAsynchronously(completionHandler: {
            print("status");
            print(assetExport.status);
            if assetExport.status == .completed {
                let asset = AVURLAsset(url: url3)
                let audioDuration = asset.duration
                let audioDurationSeconds = CMTimeGetSeconds(audioDuration)
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: [
                    "filePath": audio3,
                    "tempPath" : tempPath,
                    "duration" : audioDurationSeconds
                ]);
                self.commandDelegate!.send(pluginResult, callbackId: command.callbackId);
            } else {
                print("failed to bid");
                pluginResult = CDVPluginResult (status: CDVCommandStatus_ERROR, messageAs: assetExport.error!.localizedDescription);
                self.commandDelegate!.send(pluginResult, callbackId: command.callbackId);
            }
        })
    }
    // Send the function result back to Cordova.
  }

//Trim function start here
    @objc(trim:) // Declare your function name.
    func trim(command: CDVInvokedUrlCommand) {
        var pluginResult = CDVPluginResult (status: CDVCommandStatus_ERROR, messageAs: "The Plugin Failed");
        
        // write the function code.
        let audio1 = command.arguments[0] as? String ?? ""
        let audio2 = command.arguments[1] as? String ?? ""
        let endTime = command.arguments[2] as? Float64 ?? 0
        let tempPath = command.arguments[3] as? String ?? ""
        var error = "";
        if(audio1=="") {
            error = "Audio 1 url is missing or invalid.";
        }
        if(audio2=="") {
            error = "Audio 3 url is missing or invalid.";
        }
        if(tempPath=="") {
                error = "Temp Path url is missing or invalid.";
        }
        if(error != "") {
            pluginResult = CDVPluginResult (status: CDVCommandStatus_ERROR, messageAs: error);
            self.commandDelegate!.send(pluginResult, callbackId: command.callbackId);
        }
        let url1 = URL(string: audio1)!
        let url2 = URL(string: audio2)!
        let asset = AVAsset.init(url: url1);
        
        let compatiblePresets = AVAssetExportSession.exportPresets(compatibleWith:asset)
        
        if compatiblePresets.contains(AVAssetExportPresetLowQuality) {
            
            guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetAppleM4A) else{return}
            // Creating new output File url and removing it if already exists.
            exportSession.outputURL = url2
            exportSession.outputFileType = AVFileType.m4a
            
            let start: CMTime = CMTimeMakeWithSeconds(0, asset.duration.timescale)
            let stop: CMTime = CMTimeMakeWithSeconds(endTime, asset.duration.timescale)
            let range: CMTimeRange = CMTimeRangeFromTimeToTime(start, stop)
            exportSession.timeRange = range
            
            exportSession.exportAsynchronously(completionHandler: {
                print("status");
                print(exportSession.status);
                if exportSession.status == .completed {
                    pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: [
                        "filePath": audio2,
                        "tempPath" :tempPath
                    ]);
                    self.commandDelegate!.send(pluginResult, callbackId: command.callbackId);
                } else {
                    print("failed to bid");
                    pluginResult = CDVPluginResult (status: CDVCommandStatus_ERROR, messageAs: exportSession.error!.localizedDescription);
                    self.commandDelegate!.send(pluginResult, callbackId: command.callbackId);
                    
                }
            })
        }
    }
}

extension AVMutableCompositionTrack {
    func append(url: URL) {
        let newAsset = AVURLAsset(url: url)
        let range = CMTimeRangeMake(kCMTimeZero, newAsset.duration)
        let end = timeRange.end
        print(end)
        if let track = newAsset.tracks(withMediaType: AVMediaType.audio).first {
            try! insertTimeRange(range, of: track, at: end)
        }
        
    }
}

